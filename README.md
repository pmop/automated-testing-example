# Introduction
Simple example of automated testing of a [web application](http://selenium-blog.herokuapp.com) using Selenium, Ruby
and RSpec.

Also, [Page Object pattern](https://martinfowler.com/bliki/PageObject.html) is applied in order to
expose web page functionalities outside of the testing context, enabling reuse of code and
preventing brittle tests.

Since testing trough the WebDriver runs considerably slower than usual unit
tests, the amount of testing we want to do has to be limited in order to run
in a timely manner. [Selenium Grid](https://www.selenium.dev/documentation/en/grid/) can help improving test
times by enabling distribution of test load to worker nodes. In addition, Grid provides much wanted test environment
isolation, and also allows us to easily run tests in different settings.

# Running
- Download Firefox Selenium WebDriver and place it into your system PATH variable
- Have Ruby and Ruby bundler installed in your computer

Then using your terminal or command prompt:
- `git clone https://gitlab.com/pmop/automated-testing-example` 
- `cd automated-testing-example`
- Install required gems by running `bundle install`
- Run a test by invoking `rspec ./spec/some_spec.rb`

Where **some_spec.rb** is any file under spec folder.