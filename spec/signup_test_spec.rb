require 'rspec'
require_relative '../page_objects//_up_page'
require_relative '../page_objects/sign_up_success_page'
require 'selenium-webdriver'

describe 'Given Blog application' do
  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
  end
  after(:each) do
    @driver.quit
  end
  context 'When user signs up' do
    it 'Expect user is signed up' do

      time_stamp = Time.now.to_i

      username = "delete_me_#{time_stamp}"
      user_password = 'aaa'
      user_email = "#{time_stamp}@a.a"

      sign_up = SignUpPage.new(driver)

      sign_up.set_user_name username
      sign_up.set_user_email user_email
      sign_up.set_user_password user_password

      signed_in = sign_up.sign_up
      expect(signed_in.signed_up_banner).to eq("Welcome to the alpha blog #{username}")

    end
  end
end
