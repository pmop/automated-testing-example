require 'rspec'
require 'selenium-webdriver'
require_relative '../page_objects/login_page'

describe 'Logged in Blog' do

  # Create a new browser instance for each test
  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
  end

  # Destroy browser instance
  # Also has the effect making sure browser closes should anything go wrong
  after(:each) do
    @driver.quit
  end

  context 'User creates a new article' do
    it 'Expects article is created' do

      TITLE = 'Example article title'
      DESC = 'Example article desc'
      BANNER_EXPECTED_TEXT = 'Article was successfully created'


      login_page = LoginPage.new(@driver)
      logged_in = login_page.login_with_default_user
      articles_page = logged_in.articles
      create_article_page = articles_page.create_article

      create_article_page.set_article_title(TITLE)
      create_article_page.set_article_desc(DESC)
      article_created = create_article_page.submit

      expect(article_created.banner_text).to eq(BANNER_EXPECTED_TEXT)
      expect(article_created.title).to eq("Title: #{TITLE}")
      expect(article_created.description).to eq(DESC)

    end
  end
end
