require 'rspec'
require 'selenium-webdriver'
require_relative 'page_objects/login_page'

describe 'Blog website' do
  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
  end
  after(:each) do
    @driver.quit
  end
  context 'When user logs in' do
    it 'Expect user is logged in' do
      BANNER_TEXT = "You have successfully logged in!"

      login_page = LoginPage.new(@driver)
      logged_in_page = login_page.login_with_default_user

      expect(logged_in_page.banner_text).to eq(BANNER_TEXT)

    end
  end
end
