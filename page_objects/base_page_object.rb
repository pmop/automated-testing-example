# Base Page object implements basic Page Object functionality
class BasePageObject
  attr_reader :driver

  def initialize(driver)
    @driver = driver
    initiate_page_elements
  end

  # Automatically catches selector errors in all page object initializations
#  def page_initialization
#      begin
#        initiate_page_elements
#      rescue NotImplementedError
#        puts 'initiate_page_elements must be overridden!'
#      rescue Selenium::WebDriver::Error::NoSuchElementError, Selenium::WebDriver::Error::InvalidSelectorError => ex
#        puts "\u001b[33m#WARNING: #{ex.message}\u001b[0m"
#      end
#  end

  # Abstract method - all subclasses should implement it
  # Initializes page object elements
  def initiate_page_elements
    raise NotImplementedError
  end

end
