require_relative 'base_page_object'
require_relative 'created_article_page'

class CreateArticlePage < BasePageObject

  #CSS selectors
  ARTICLES_TITLE = {id: "article_title"}
  ARTICLES_DESC = {id: "article_description"}
  ARTICLES_SUBMIT = {name: "commit"}

  # Find web page elements, so we can expose its functionalities
  def initiate_page_elements
    current_url = @driver.current_url
    if current_url != "http://selenium-blog.herokuapp.com/articles/new"
      raise "Unexpected url. Expected http://selenium-blog.herokuapp.com/articles/new got #{current_url}"
    end

    wait = Selenium::WebDriver::Wait.new(:timeout => 2)
    @article_title_field = wait.until{@driver.find_element(ARTICLES_TITLE)}
    @article_desc = @driver.find_element(ARTICLES_DESC)
    @article_submit = @driver.find_element(ARTICLES_SUBMIT)
  end

  #Expose web page functionality

  def set_article_title(title)
    @article_title_field.send_keys(title)
  end

  def set_article_desc(desc)
    @article_desc.send_keys(desc)
  end

  def submit
    @article_submit.click

    CreatedArticlePage.new(@driver)
  end

end
