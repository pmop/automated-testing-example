require_relative 'base_page_object'

class CreatedArticlePage < BasePageObject

  #CSS selectors
  CREATED_SUCCESS = {id: "flash_success"}
  CREATED_ARTICLE_TITLE = {css: "div > h2"}
  CREATED_ARTICLE_DESCRIPTION = {css: "div > p"}

  # Find web page elements, so we can expose its functionalities
  def initiate_page_elements

    current_url = @driver.current_url
    if not (/http:\/\/selenium-blog.herokuapp.com\/articles\/\d+/.match? current_url)
      raise "Unexpected url. Expected http://selenium-blog.herokuapp.com/articles/\d+ got #{current_url}"
    end
    @article_banner = @driver.find_element(CREATED_SUCCESS)
    @article_title = @driver.find_element(CREATED_ARTICLE_TITLE)
    @article_desc = @driver.find_element(CREATED_ARTICLE_DESCRIPTION)
  end

  #Expose web page functionality

  def title
    @article_title.text || 'Not found'
  end

  def description
    @article_desc.text || 'Not found'
  end

  def banner_text
    @article_banner.text || 'Not found'
  end

end
