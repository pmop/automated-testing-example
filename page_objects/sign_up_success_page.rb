require_relative 'base_page_object'

class SignUpSuccessPage < BasePageObject
  FLASH_SUCCESS = {id: "flash_success"}

  def initiate_page_elements
    @alert_success = @driver.find_element(FLASH_SUCCESS)
  end

  def signed_up_banner
    @alert_success.text || 'Not found'
  end
end
