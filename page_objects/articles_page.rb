require_relative 'base_page_object'
require_relative 'create_article_page'

class ArticlesPage < BasePageObject

  #CSS selectors
  ARTICLES_NEW = {xpath: "/html/body/div[2]/p/a"}

  # Find web page elements, so we can expose its functionalities
  def initiate_page_elements
    current_url = @driver.current_url
    if current_url != "http://selenium-blog.herokuapp.com/articles"
      raise "Unexpected url. Expected http://selenium-blog.herokuapp.com/articles got #{current_url}"
    end

    wait = Selenium::WebDriver::Wait.new(:timeout => 2)
    @articles_new = wait.until{@driver.find_elements(:xpath, "/html/body/div[2]/p/a")[0]}
  end

  #Expose web page functionality

  def create_article

    @articles_new.click
    CreateArticlePage.new(@driver)
  end

end
