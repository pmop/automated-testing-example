require_relative 'base_page_object'
require_relative 'sign_up_success_page'

class SignUpPage < BasePageObject

  #CSS selectors
  USER_NAME = {id: "user_name"}
  EMAIL = {id: "user_email"}
  PASSWORD = {id: "user_password"}
  SUBMIT = {id: "user_password"}

  # Find web page elements, so we can expose its functionalities
  def initiate_page_elements
    @driver.navigate.to "http://selenium-blog.herokuapp.com/signup"
    @user_name_input_field = @driver.find_element(USER_NAME)
    @user_email_input_field = @driver.find_element(EMAIL)
    @user_password_input_field = @driver.find_element(PASSWORD)
    @submit_button = @driver.find_element(SUBMIT)
  end

  #Expose web page functionality

  def set_user_name(user_name)
    @user_name_input_field.send_keys(user_name)
  end

  def set_user_email(user_email)
    @user_email_input_field.send_keys(user_email)
  end

  def set_user_password(user_pass)
    @user_password_input_field.send_keys(user_pass)
  end

  def sign_up
    @submit_button.click

    SignUpSuccessPage.new(@driver)
  end
end
