require_relative 'base_page_object'
require_relative 'articles_page'


# I think that it is better to check if user is logged in, in the tests, instead of creating a page object for it
class LoggedInPage < BasePageObject
  LOGGED_IN_BANNER = {id: "flash_success"}
  ARTICLES = {link_text: 'Articles'}

  def initiate_page_elements
    current_url = @driver.current_url
    wait = Selenium::WebDriver::Wait.new(:timeout => 2)
      if not (/http:\/\/selenium-blog.herokuapp.com\/users\/\d+/.match? current_url)
        raise "Unexpected url. Expected http://selenium-blog.herokuapp.com/users/\d{4} got #{current_url}"
      end
      @login_banner_success = wait.until{@driver.find_element(LOGGED_IN_BANNER)}
      @articles_button = @driver.find_element(ARTICLES)
  end

  def articles
    @articles_button.click
    ArticlesPage.new(@driver)
  end

  def banner_text
    @login_banner_success.text || 'Not found'
  end
end
