require_relative 'base_page_object'
require_relative 'logged_in_page'

class LoginPage < BasePageObject

  # Form selectors
  EMAIL = {id: "session_email"}
  PASSWORD = {id: "session_password"}
  LOGIN_BUTTON = {name: "commit"}

  DEFAULT_EMAIL = 'a@a.a'
  DEFAULT_PASSWORD = 'aaa'

  def initiate_page_elements
    @driver.navigate.to 'http://selenium-blog.herokuapp.com/login'

    @email_field = @driver.find_element(EMAIL)
    @password_field = @driver.find_element(PASSWORD)
    @login_button = @driver.find_element(LOGIN_BUTTON)
  end

  def set_password(passd)
    @password_field.send_keys(passd)
  end

  def set_email(email)
    @email_field.send_keys(email)
  end

  def login
    @login_button.click
  end

  def login_with_default_user
    set_email DEFAULT_EMAIL
    set_password DEFAULT_PASSWORD

    login

    LoggedInPage.new(@driver)
  end
end
